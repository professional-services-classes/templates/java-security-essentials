## Security Essentials Lab Test 
This project is for the use of Professional Services Security Essentials Class.  If you would like to make a change to this project Please notify Professional Services via our [PS Web Form](https://about.gitlab.com/services/#contactform)

If you are a GitLab employee, you can [Create an Issue](https://gitlab.com/gitlab-com/customer-success/professional-services-group/trainings/training-feedback)

###
Need to replace several URLs in the project files to the URL in the production environment:
 - `src/test/load-test.js`
 - `src/test/testcalls.jmx`

The full .gitlab-ci.yml is located [In Snippets in this project](https://gitlab-core.us.gitlabdemo.cloud/training-sample-projects/java-security-essentials/-/snippets/5384)

Further instructions for this demo can be found:
- [Security Essentials](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson.html)

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

